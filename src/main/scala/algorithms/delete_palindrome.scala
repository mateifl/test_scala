package algorithms
import scala.collection.mutable.Map
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

object Recurrence {

  def apply( inputString : String, startIndex : Int, endIndex : Int, letterPositions : Map[Char, ListBuffer[Int]]) : Int = {
//    println(startIndex, endIndex)
    if(endIndex == startIndex)
      0 // a 1 letter string is a palindrome
    else if (endIndex == startIndex - 1)
    {
      if(inputString(startIndex) == inputString(endIndex))
        0 // we already have a palindrome
      else
        1 // we delete any letter and we have a palindrome
    }
    else
    {
      // last letter not in max palindrome so we delete it
      var s1 = apply(inputString, startIndex, endIndex - 1, letterPositions) + 1
      // first letter not in max palindrome so we delete it
      val s2 = apply(inputString, startIndex + 1, endIndex, letterPositions) + 1
      if (s1 > s2) s1 = s2
      // last letter is in max palindrome
      breakable {
        for (index <- letterPositions(inputString.charAt(endIndex))) {
          if (index >= endIndex)
            break
          val s = apply(inputString, index + 1, endIndex - 1, letterPositions) + index - startIndex
          if (s < s1)
            s1 = s
        }
      }
      // first letter is in max palindrome
      breakable {
        for (index <- letterPositions(inputString.charAt(endIndex)).reverse) {
          if (index <= endIndex)
            break
          val s = apply(inputString, startIndex + 1, index - 1, letterPositions) + endIndex - index
          if (s < s1)
            s1 = s
        }
      }
      println(startIndex, endIndex, s1)
      s1
    }
  }
}

object DeletePalindrome {

  def apply(inputString : String): Unit = {
    val stringLen = inputString.length
    val memo = Array.fill(stringLen, stringLen)(1000000)

    for(i <- 0 until stringLen)
      memo(i)(i) = 0
    for(i <- 0 until stringLen - 1)
      memo(i)(i + 1) = if (inputString(i) == inputString(i + 1)) 0 else 1

    memo.foreach(x => println(x.toList))

    for(i <- 0 until stringLen)
      for(j <- 0 until i  )
        memo(i)(j) = 0

  }
}

object Main {
  def main(args: Array[String]): Unit = {
    val inputString = args(0)
    val m = getIndices(inputString)
    println(m)
    val r = Recurrence(inputString, 0, inputString.length - 1, m)
    println("Done", r)

    println("ITERATIVE")
    DeletePalindrome(inputString)
  }

  private def getIndices(inputString : String) : Map[Char, ListBuffer[Int]] = {
    val m = Map.empty[Char, ListBuffer[Int]]
    var i = 0
    for ( c <- inputString )
    {
      if (!m.contains(c)) m(c) = new ListBuffer[Int]
      m(c) += i
      i += 1
    }
    m
  }

}
