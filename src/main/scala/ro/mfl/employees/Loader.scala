package ro.mfl.employees
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext

class Loader {
  val conf = new SparkConf().setAppName("stack overflow")
  val sc = new SparkContext(conf)
  val sqlContext = new SQLContext(sc)

  def load(): (RDD[(Int, String)], RDD[(Int, String)], RDD[(String, String)]) = {
    val employeesRDD = sc.textFile("/home/matei/data/test_db-master/employees.csv")
                         .map(x => x.split("\t"))
                         .map(x => (x(0).toInt, x(1)))

    val departmentsRDD = sc.textFile("/home/matei/data/test_db-master/departments.csv")
                        .map(x => x.split("\t"))
                        .map(x => (x(0), x(1)))

    val linkRDD = sc.textFile("/home/matei/data/test_db-master/dept_emp.csv")
                    .map(x => x.split("\t"))
                    .map(x => (x(0).toInt, x(1)))

    (employeesRDD, linkRDD, departmentsRDD)
  }

  def createDataFrames(rdd: RDD[(Int, String)]): Unit = {
    import sqlContext.implicits._
    rdd.toDF()
  }
}

object Test extends App {
  val l = new Loader
  val rdds = l.load()
  println("Joining....")
  rdds._1.cache
  rdds._2.cache
  rdds._3.cache

  val firstJoin = rdds._1.join(rdds._2).map(x => (x._2._2, (x._1, x._2._1)))
  firstJoin.cache
  println(firstJoin.count())
  val secondJoin = firstJoin.join(rdds._3)
  secondJoin.cache
  secondJoin.groupByKey().foreach(x => println(x._1 + " " + x._2.size))

}