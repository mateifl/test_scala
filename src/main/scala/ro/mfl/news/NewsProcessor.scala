package ro.mfl.news

import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer}

object NewsProcessor {
  def main(args: Array[String]): Unit = {

    val jsonParser = new JSONParser
    val source = Source.fromFile("c:\\tmp\\News_Category_Dataset_v2.json")

    val newsItems = jsonParser.processFile(source)
    println(newsItems.size)

    val conf = new SparkConf().setAppName("news processor")
    val sparkSession = SparkSession.builder
      .master("local")
      .appName("news process")
      .getOrCreate()

    val df = sparkSession.createDataFrame(newsItems)
    df.show(10)
    val tokenizer = new Tokenizer().setInputCol("shortDescription").setOutputCol("words")
    val wordsData = tokenizer.transform(df)
    wordsData.show(10)
  }
}
