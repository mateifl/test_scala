package ro.mfl.news

import java.io.StringReader
import javax.json.Json
import javax.json.stream.JsonParser

import scala.io.Source

class JSONParser {

  def processFile(source : Source) : List[NewsItem] = {
    val lines = source.getLines.toList.map( x => toNewsItem(x) )
    source.close()
    lines
  }

  def toNewsItem(line: String) : NewsItem = {
    val parser = Json.createParser(new StringReader(line))
    var values = Map[String, String]()
    while (parser.hasNext)
    {
      val event = parser.next()

      event match  {
        case JsonParser.Event.KEY_NAME =>
          val key = parser.getString
          parser.next
          val value = parser.getString
          values += (key -> value)
        case _ =>
      }
    }

    NewsItem(values.getOrElse("category", ""), values.getOrElse("headline", ""), values.getOrElse("authors", ""), values.getOrElse("link", ""), values.getOrElse("short_description", ""), values.getOrElse("date", ""))
  }
}

case class NewsItem(category: String, headline: String, authors: String, link: String, shortDescription: String, date: String)