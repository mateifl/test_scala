package ro.mfl.stackdata
import java.util.Calendar
import java.sql.Timestamp

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}

object LineProcessor1 {

  def apply(line: Array[String]): (Int, Int, Int, Int, String) = {
    (line.head.toInt, line(2).toInt, line(3).toInt, line(4).toInt, line(5))
  }
}
