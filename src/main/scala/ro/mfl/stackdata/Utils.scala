package ro.mfl.stackdata
import java.io._

import org.apache.spark.sql.DataFrame

import scala.io.Source

object PropertyFileReader {
  def apply() : Map[String, String] = {
    val path = System.getProperty("user.dir") + "/" + "config.properties"
    val source = Source.fromFile(path)
    val lines =  source.getLines.toList.map(x => x.split("=")).map(x => x(0) -> x(1)).toMap
    source.close()
    lines
  }
}

object DataFrameToCSV {
  val dataFolderPath = PropertyFileReader()("data_path")
  def apply(dataFrame: DataFrame, path: String, separator: String = "\t") = {
    val file = new File(dataFolderPath + File.separator + path)
    val writer = new BufferedWriter(new FileWriter(file))
    val rows = dataFrame.collect()

    for (row <- rows) {
      val s = row.toString().replace("[", "").replace("]", "")
      writer.write(s + "\n")
    }
    writer.close()
  }
}

object isInteger {
  def apply(s: String) : Boolean = {
    try {
      s.toInt
      true
    }
    catch {
      case _ : Throwable =>
      false
    }
  }
}


object SeparateAnswers {

  val dataFolder = PropertyFileReader()("data_path")

  def apply(): Unit = {
    val file = Source.fromFile(dataFolder + File.separator + "answers_spark.csv")
    val lines = file.getLines.map(_.split("\t"))
    val partitions = lines.partition(x => isInteger(x.last))
//    println(partitions._1.size)
//    println(partitions._2.size)
    val writer = new BufferedWriter(new FileWriter(dataFolder + File.separator + "answers_user_id.tsv"))
    partitions._1.foreach(x => writer.write(x.mkString("\t") + "\n"))
    writer.close

    val writer2 = new BufferedWriter(new FileWriter(dataFolder + File.separator + "answers_user_name.tsv"))
    partitions._2.foreach(x => writer2.write(x.mkString("\t") + "\n"))
    writer2.close
  }
}


class SelectColumnsFromFile {

  def selectColumnsToFile(filename: String, newFilename: String, columns: Set[Int]): Unit = {
    val file = Source.fromFile(filename)
    val lines = file.getLines().map(_.split("\t")).map(tokensByIndexToString(_, columns))
    val writer = new BufferedWriter(new FileWriter(newFilename))
    lines.foreach(writer.write(_))
    file.close
    writer.close
  }

  def selectColumns(filename: String, columns: Set[Int]): List[List[String]] = {
    val file = Source.fromFile(filename)
    val lines = file.getLines().map(x => tokensByIndexToList(x.split("\t"), columns) ).toList
    file.close
    lines
  }

  private def tokensByIndexToString(line: Array[String], indices: Set[Int]) : String  = {
    line.zipWithIndex.filter( x => indices.contains(x._2) ).map(_._1).mkString("\t")
  }

  private def tokensByIndexToList(line: Array[String], indices: Set[Int]) : List[String]  = {
    line.zipWithIndex.filter( x => indices.contains(x._2) ).map(_._1).toList
  }
}


object TestUtils extends App {
  override def main(args: Array[String]): Unit = {
    val configs = PropertyFileReader()
    configs.foreach(x => println(x._1 + " " + x._2))
  }
}

object SeparateAnswersApp extends App {

  override def main(args: Array[String]): Unit = {
    SeparateAnswers()

  }

}