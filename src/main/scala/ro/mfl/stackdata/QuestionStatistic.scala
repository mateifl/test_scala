package ro.mfl.stackdata

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import java.io._

case class QuestionObject(id: Int, info: String)
case class TagObject(id: Int, info: String)

class QuestionStatistics(val sc: SparkContext, val folderPath: String) {

  def writeRDD[T](rdd : RDD[T], filePath: String): Unit = {
    val fileWriter = new BufferedWriter(new FileWriter(folderPath + filePath))
    for(x <- rdd.collect)
      fileWriter.write(x.toString + "\n")
    fileWriter.close()
  }

  def create(questionFilePath: String, tagQuestionFilePath: String, tagFilePath: String) = {
    val questionRDD = sc.textFile(folderPath + questionFilePath)
                        .map(x => (x.split("\t")(0).toInt, x))
                        .map(x => (x._1, QuestionObject(x._1, x._2)))
//    questionRDD.cache
//    writeRDD(questionRDD, "q_test.csv")
    val tagQuestionRDD = sc.textFile(folderPath + tagQuestionFilePath)
                           .map(x => (x.split("\t")(0).toInt, x))
    val merged = questionRDD.join(tagQuestionRDD).map(x => x._2).map(x => (x._2.split("\t")(1).toInt, x._1))
//    merged.cache
//    writeRDD(merged, "merged_test.csv")
    // (QuestionObject, tag question line) -> (tag id, QuestionObject)
    val tagsRDD = sc.textFile(folderPath + tagFilePath)
                    .map(x => (x.split("\t")(0).toInt, x))
                    .map(x => (x._1, TagObject(x._1, x._2)))
    val merged2 = merged.join(tagsRDD).map(x => x._2).map(x => x._1.info + " " + x._2.info)
    val fileWriter = new BufferedWriter(new FileWriter(folderPath + "merged.csv"))
    merged2.collect.foreach(x => fileWriter.write(x + "\n"))
    fileWriter.close()
  }

}

object QuestionStatsBuilder extends App {
  override def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("stack overflow")
    val sc = new SparkContext(conf)
    val path = "D:\\data\\stackoverflow\\"
    val stat = new QuestionStatistics(sc, path)
    stat.create("questions_spark.csv", "tag_question_spark.csv", "tags_spark.csv")

  }
}
