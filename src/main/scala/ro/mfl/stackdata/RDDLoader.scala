package ro.mfl.stackdata

import java.sql.Timestamp
import java.util.Calendar

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import ro.mfl.stackdata.EntityType.EntityType

import scala.reflect.ClassTag

object EntityType extends Enumeration {
  type EntityType = Value
  val Answer, AnswerAccountId, Question, Tag, TagQuestion, User = Value
}

trait StackEntity {
  val id: Int
  def stringRepr: String
}

trait LinkEntity[T] extends StackEntity {
  val secondId: Int
}

trait Builder[T <: StackEntity] extends Serializable {
  def create(line: Array[String]) : T
}

object SetDate {
  def apply(dateString: String): Calendar = {
    val date_split = dateString.split("-")
    val date = Calendar.getInstance
    date.setTimeInMillis(0)
    date.set(Calendar.YEAR, date_split(0).toInt)
    date.set(Calendar.MONTH, date_split(1).toInt)
    date.set(Calendar.DAY_OF_MONTH, date_split(2).toInt)
    date
  }
}

class TagQuestionLinkBuilder extends Builder[TagQuestionLink] {
  def create(line: Array[String]) = TagQuestionLink(line.head.toInt, line(1).toInt)
}

case class TagQuestionLink(id: Int, secondId: Int) extends LinkEntity[TagQuestionLink] {
  def stringRepr : String = {
    List(id, secondId).mkString("\t")
  }
}

class QuestionBuilder extends Builder[Question] {

  def create(line: Array[String]) : Question = {
    val date = SetDate(line(1))
    Question(line.head.toInt, new Timestamp(date.getTimeInMillis),
             line(2).toInt, line(3).toInt, line(4).toInt, line(5), date.get(Calendar.WEEK_OF_YEAR))
  }
}

case class Question(id: Int, date: Timestamp, answers: Int, score: Int, viewCount: Int, user: String, week: Int)
  extends StackEntity {
  def stringRepr : String = {
    List(id, date, answers, score, viewCount, user, week).mkString("\t")
  }
}

class AnswerBuilder extends Builder[Answer] {
  def create(line: Array[String]) =
  {
    val date = SetDate(line(1))
    Answer(line.head.toInt, new Timestamp(date.getTimeInMillis),
           line(2).toInt, line(3).toInt, line(4), date.get(Calendar.WEEK_OF_YEAR))
  }
}

class AnswerWithIdBuilder extends Builder[AnswerAccountId] {
  def create(line: Array[String]) = {
    val date = SetDate(line(1))
    AnswerAccountId(line.head.toInt, new Timestamp(date.getTimeInMillis),
      line(2).toInt, line(3).toInt, line(4).toInt, date.get(Calendar.WEEK_OF_YEAR))
  }
}

case class Answer(id: Int, date: Timestamp, questionId: Int, score: Int, user: String, week: Int)
  extends StackEntity
{
  def stringRepr : String = List(id, date, questionId, score, user, week).mkString("\t")
}

case class AnswerAccountId(id: Int, date: Timestamp, questionId: Int, score: Int, accountId: Int, week: Int)
  extends StackEntity
{
  def stringRepr : String = List(id, date, questionId, score, accountId, week).mkString("\t")
}

class TagBuilder extends Builder[Tag] {
  def create(line: Array[String]) = Tag(line.head.toInt, line(1))
}

case class Tag(id: Int, tag: String) extends StackEntity {
  def stringRepr : String = {
    List(id, tag).mkString("\t")
  }
}

case class User(id: Int, reputation: Int, date: Timestamp, displayName: String, accountId: Int) extends StackEntity {
  def stringRepr : String = {
    List(id, reputation, date, displayName, accountId).mkString("\t")
  }
}

class UserBuilder extends Builder[User] {
  def create(line: Array[String]) = {
    val date = SetDate(line(2))
    User(line.head.toInt, line(1).toInt, new Timestamp(date.getTimeInMillis), line(3), line(4).toInt)
  }
}

class FileLoader() extends Serializable {
  val path = PropertyFileReader()("data_path")
  val conf = new SparkConf().setAppName("stack overflow")

  def loadDF(entityType: EntityType, fileName: String, tableName: String, sc: SparkContext, sqlContext: SQLContext): Unit = {
    import sqlContext.implicits._

    entityType match
    {

      case EntityType.AnswerAccountId =>
        val builder = new AnswerWithIdBuilder
        val rdd = loadRDD[AnswerAccountId, AnswerWithIdBuilder](fileName, builder, sc)
        rdd.toDF().registerTempTable(tableName)
      case EntityType.Answer =>
        val builder = new AnswerBuilder
        val rdd = loadRDD[Answer, AnswerBuilder](fileName, builder, sc)
        rdd.toDF().registerTempTable(tableName)
      case EntityType.Question => //loadRDD[Question, QuestionBuilder](path + fileName, new QuestionBuilder, sc)
        val builder = new QuestionBuilder
        val rdd = loadRDD[Question, QuestionBuilder](fileName, builder, sc)
        rdd.toDF().registerTempTable(tableName)
      case EntityType.User =>
        val builder = new UserBuilder
        val rdd = loadRDD[User, UserBuilder](fileName, builder, sc)
        rdd.toDF().registerTempTable(tableName)
      case EntityType.TagQuestion =>
        val builder = new TagQuestionLinkBuilder
        val rdd = loadRDD[TagQuestionLink, TagQuestionLinkBuilder](fileName, builder, sc)
        rdd.toDF().registerTempTable(tableName)
      case EntityType.Tag =>
        val builder = new TagBuilder
        val rdd = loadRDD[Tag, TagBuilder](fileName, builder, sc)
        rdd.toDF().registerTempTable(tableName)
    }
  }

  private def loadRDD[T <: StackEntity, U <: Builder[T]](fileName: String, builder: U, sc: SparkContext)(implicit c: ClassTag[T]) : RDD[T] = {
    val rdd = sc.textFile(path + fileName).map(x => builder.create(x.split("\t")))
    rdd
  }

}
