package ro.mfl.stackdata

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

import scala.util.matching.Regex
import java.io._

object PostLineProcessor {
  val pattern = """\sId="(\d+)".*PostTypeId="(\d+)".*CreationDate="(\d{4}-\d{2}-\d{2})""".r
  val questionPattern = """\sScore="(-*\d+)".*ViewCount="(\d+)".*Tags="(\S*)".*AnswerCount="(\d+)"""".r
  val answerPattern = """\sParentId="(\d+)".*Score="(-*\d+)"""".r

  def apply(line: String): String = {
    try {
      if (!line.contains("<row")) {
        println("not posts line")
        return ""
      }

      val matchData = pattern.findAllIn(line).matchData.map(m => m.subgroups).toList.head
      val postTypeId = matchData(1).toInt
      if(postTypeId == 1) {
        val questionData = questionPattern.findAllIn(line).matchData.map(m => m.subgroups).toList.head
        return matchData.mkString("\t") + "\t" + processQuestionLine(questionData)
      }
      else if(postTypeId == 2) {
        val answerData = answerPattern.findAllIn(line).matchData.map(m => m.subgroups).toList.head
        return matchData.mkString("\t") + "\t" + answerData.mkString("\t")
      }
      ""
    }
    catch {
      case e : Exception =>
        println(line)
        e.printStackTrace()
    }
    ""
  }

  private def processQuestionLine(questionData: List[String]) : String = {
    val tags = questionData(2).substring(4, questionData(2).length - 4).replace("&gt;&lt;", ",")
    questionData(0) + "\t" + questionData(1) + "\t" + tags + "\t" + questionData(3)
  }
}


object LineProcessor {

  private val pattern = """\sId="(\d+)".*Reputation="(\d+)".*CreationDate="(\d{4}-\d{2}-\d{2}).*DisplayName="([^"]*)".*AccountId="(\d+)"""".r
  private val c = 0x2028
  def apply(line: String): String = {
    try {
      if (!line.contains("<row")) {
        println("not user line")
        return ""
      }
      val cleanLine = line.replace(c.toChar, ' ')
      val matchData = pattern.findAllIn(cleanLine).matchData.map(m => m.subgroups).toList.head
      return matchData.mkString("\t")
    }
    catch {
      case e : Exception =>
        println(e.getMessage)
        println(line)
    }
    ""
  }
}

class UsersProcessor(sc: SparkContext) {

  def process(filePath: String, resultFilePath: String) = {
    val usersRDD = sc.textFile(filePath).map(line => LineProcessor(line))
    val lines = usersRDD.collect()
    val fileWriter = new BufferedWriter(new FileWriter(resultFilePath))
    lines.foreach(line => fileWriter.write(line + "\n"))
    fileWriter.close()
  }
}

class PostsProcessor(sc: SparkContext) {

  def process(filePath: String, questionsFilePath: String, answersFilePath: String) = {
    val rdd = sc.textFile(filePath).map(line => PostLineProcessor(line))
    rdd.cache()
    val lines = rdd.collect().filter(x => !x.isEmpty).partition(x => x.split("\t")(1).toInt == 1)

    val questionsFileWriter = new BufferedWriter(new FileWriter(questionsFilePath))
    lines._1.foreach(line => questionsFileWriter.write(line + "\n"))
    questionsFileWriter.close()

    val answersFileWriter = new BufferedWriter(new FileWriter(answersFilePath))
    lines._2.foreach(line => answersFileWriter.write(line + "\n"))
    answersFileWriter.close()
  }
}

object Processor extends App {
  override def main(args: Array[String]) {
    val path = PropertyFileReader()("data_path") + File.separator + "Posts-small.txt"
    val qPath = PropertyFileReader()("data_path") + "q_spark.csv"
    val aPath = PropertyFileReader()("data_path") + "a_spark.csv"
    val conf = new SparkConf().setAppName("stack overflow")
    val sc = new SparkContext(conf)
    val up = new PostsProcessor(sc)
    up.process(path, qPath, aPath)
  }
}
