package ro.mfl.stackdata
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}
import ro.mfl.stackdata.EntityType.EntityType

class QuestionsSelector(sqlContext: SQLContext) {

  def query(sqlQuery: String) = {
    val selectedDf = sqlContext.sql(sqlQuery)
    selectedDf
  }

}

import org.apache.spark.sql.types.{StructType, StructField, StringType, IntegerType, DateType}

object ArbitraryFileLoader {

  def apply(fileName: String, tableName: String, types: Array[Int], sc: SparkContext, sqlContext: SQLContext) = {

    import sqlContext.implicits._
    var i = 0
    val fieldsArray = types.map( x => StructField("col" + (i = i + 1),
                                                 x  match {
                                                   case 0 => StringType
                                                   case 1 => IntegerType
                                                   case 2 => DateType
                                                 },
                                                 true)
                              )


//    val types = new

    val schema = StructType(fieldsArray)
    val rdd = sc.textFile(fileName).map(_.split("\t")).map(x => Row(x))
    val dataFrame = sqlContext.createDataFrame(rdd, schema)
    dataFrame.registerTempTable(tableName)
  }

}


case class EntityDescriptor(entityType: EntityType, table: String, entityName: String)

object Selector extends App {
  override def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("stack overflow")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    val loader = new FileLoader

    load(loader, EntityDescriptor(EntityType.Question, "questions", "questions"), PropertyFileReader()("questions_file"), sc, sqlContext)
    load(loader, EntityDescriptor(EntityType.AnswerAccountId, "answers", "answers"), PropertyFileReader()("answers_file"), sc, sqlContext)
    load(loader, EntityDescriptor(EntityType.Tag, "tags", "tags"), PropertyFileReader()("tags_file"), sc, sqlContext)
    load(loader, EntityDescriptor(EntityType.TagQuestion, "tags_questions", "tags questions link"), PropertyFileReader()("questions_tags_file"), sc, sqlContext)
    load(loader, EntityDescriptor(EntityType.User, "users", "users"), PropertyFileReader()("users_file"), sc, sqlContext)

    loadArbitraryFiles(sc, sqlContext)

    println("Data loaded")

    val selector = new QuestionsSelector(sqlContext)

    while(true)  {
      println("Query:")
      val sql = Console.readLine()
      if(sql.equalsIgnoreCase(":quit"))
        System.exit(0)
      println("File name:")
      val filename = Console.readLine()
      val df = selector.query(sql)

      println("Number of lines selected = " + df.count())

      if(!filename.isEmpty)
        DataFrameToCSV(df, filename)
    }
  }

  private def load(loader: FileLoader, entityDescriptor: EntityDescriptor, fileName: String, sc: SparkContext, sqlContext: SQLContext): Unit =
  {
    println("Load " + entityDescriptor.entityName +  "?")
    val answer = Console.readLine()
    if(answer.equalsIgnoreCase("yes"))
      loader.loadDF(entityDescriptor.entityType, fileName, entityDescriptor.table, sc, sqlContext)
  }

  private def loadArbitraryFiles(sc: SparkContext, sqlContext: SQLContext) : Unit = {
    while(true) {
      println("Load arbitrary file:")
      val answer = Console.readLine()
      if (!answer.equalsIgnoreCase("yes")) return
      println("Name:")
      val name = Console.readLine()
      println("Table:")
      val table = Console.readLine()
      println("Types [0-string, 1-int, 2-date]:")
      val types = Console.readLine().split(" ").map(x => x.toInt)
      ArbitraryFileLoader(name, table, types, sc, sqlContext)
    }
  }
}
