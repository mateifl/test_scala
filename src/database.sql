select count(*) from tags
select count(*) from questions

delete FROM questions where view_count is null

load data infile 'd:\\data\\stackoverflow\\tags.csv' into table tags fields terminated by '\t' lines terminated by '\n';
load data infile 'd:\\data\\stackoverflow\\questions_spark.csv' into table questions fields terminated by '\t' lines terminated by '\n' (id, @created, answers, score, view_count, user_id) set created = str_to_date(@created, '%Y-%m-%d');
load data infile 'd:\\data\\stackoverflow\\tag_question_spark.csv' into table questions_tags fields terminated by '\t';
load data infile 'd:\\data\\stackoverflow\\answers_spark.csv' into table answers fields terminated by '\t' lines terminated by '\n' (id, @created, score, view_count, user_id) set created = str_to_date(@created, '%Y-%m-%d');


alter table questions add CONSTRAINT questions_pk primary key(id);
alter table answers add CONSTRAINT answers_pk primary key(id);
alter table tags add CONSTRAINT tags_pk primary key(id);

create index question_id_idx on questions_tags(question_id);
create index tag_id_idx on questions_tags(tag_id);
create UNIQUE index uniq_tag_name on tags(tag);
