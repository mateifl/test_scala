name := "stack_data"

version := "1.0"

scalaVersion := "2.12.4"

//val mysql = "mysql" % "mysql-connector-java" % "5.1.12"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.0",
  "org.apache.spark" %% "spark-sql" % "2.4.0",
  "org.apache.spark" %% "spark-mllib" % "2.4.0")

libraryDependencies += "javax" % "javaee-api" % "7.0" % "provided"
libraryDependencies += "javax.json" % "javax.json-api" % "1.1.4" % "provided"
libraryDependencies += "org.glassfish" % "javax.json" % "1.1.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
//libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.+"